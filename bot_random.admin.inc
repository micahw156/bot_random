<?php

/**
 * @file
 * Contains admin functionality for the bot_random module.
 *
 * @see bot_random.module
 */

/**
 * Configure random quotes.
 */
function bot_random_settings($form, &$form_state) {

  $form['bot_random_enabled'] = [
    '#title' => t('Enabled'),
    '#description' => t('Enable random quotes'),
    '#type' => 'checkbox',
    '#default_value' => variable_get('bot_random_enabled', FALSE),
  ];
  $form['bot_random_weekdays'] = [
    '#title' => t('Weekdays'),
    '#description' => t('Weekdays the bot is active.'),
    '#type' => 'checkboxes',
    '#options' => [
      7 => t('Sunday'),
      1 => t('Monday'),
      2 => t('Tuesday'),
      3 => t('Wednesday'),
      4 => t('Thursday'),
      5 => t('Friday'),
      6 => t('Saturday'),
    ],
    '#default_value' => variable_get('bot_random_weekdays', []),
  ];
  $time_opts = [];
  for ($i=0; $i < 24; $i++) {
    foreach(['00', '15', '30', '45'] as $m) {
      $time_opts[$i . $m] = $i . ':' . $m ;
    }
  }
  $form['bot_random_start_time'] = [
    '#title' => t('Start time'),
    '#type' => 'select',
    '#options' => $time_opts,
    '#default_value' => variable_get('bot_random_start_time', NULL),
  ];
  $form['bot_random_end_time'] = [
    '#title' => t('End time'),
    '#type' => 'select',
    '#options' => $time_opts,
    '#default_value' => variable_get('bot_random_end_time', NULL),
  ];
  $minutes = [];
  for ($i=1; $i < 16; $i++) {
    $minutes[$i] = $i;
  }
  $form['bot_random_interval_minimum'] = [
    '#title' => t('Minimum interval'),
    '#type' => 'select',
    '#options' => $minutes,
    '#default_value' => variable_get('bot_random_interval_minimum', 1),
  ];
  $form['bot_random_interval_maximum'] = [
    '#title' => t('Maximum interval'),
    '#type' => 'select',
    '#options' => $minutes,
    '#default_value' => variable_get('bot_random_interval_maximum', 15),
  ];

  // get a list of all joined channels, sans passwords.
  $joined_channels = preg_split('/\s*,\s*/', variable_get('bot_channels', '#test'));
  $channel_options = [];
  foreach ($joined_channels as $k => $v) {
    $channel = preg_replace('/(.*) .*/', '\1', $v);
    $channel_options[$channel] = $channel;
  }

  $form['bot_random_channels'] = [
    '#title' => t('IRC channels'),
    '#type' => 'select',
    '#description' => t('Select the channels to send random messages.'),
    '#multiple' => TRUE,
    '#options' => $channel_options,
    '#size' => 10,
    '#default_value' => variable_get('bot_random_channels', []),
  ];
  return system_settings_form($form);
}
