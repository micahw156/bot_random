<?php

/**
 * Defines the BotQuote Entity API Conroller class.
 */
class BotQuoteController extends EntityAPIController {

  /**
   * Create and return a new directory info entity.
   *
   * Overrides EntityAPIController::create().
   */
  public function create(array $values = array()) {
    global $user;

    $values = $values + array(
      'is_new' => TRUE,
      'uid' => $user->uid,
      'author' => $user->name,
      'status' => NODE_PUBLISHED,
      'created' => REQUEST_TIME,
    );
    return parent::create($values);
  }

  /**
   *
   */
  protected function attachLoad(&$entities, $revision_id = FALSE) {

    foreach ($entities as $id => $entity) {
      if (!empty($entity->uid)) {
        if ($account = user_load($entity->uid)) {
          $entities[$id]->author = $account->name;
        }
      }
    }

    parent::attachLoad($entities, $revision_id);
  }

  /**
   * Define display properties for entity contents.
   *
   * Overrides EntityAPIController::buildContent().
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {

    if (!empty($entity->message)) {
      $content['message'] = array(
        '#prefix' => '<div class="bot-quote-message">',
        '#markup' => $entity->message,
        '#suffix' => '</div>',
      );
    }

    if (!empty($entity->author)) {
      $content['author'] = array(
        '#prefix' => '<div class="bot-quote-author">',
        '#markup' => $entity->author,
        '#suffix' => '</div>',
      );
    }

    // Set the weights for the fields above.
    // @see _field_extra_fields_pre_render()
    $extra_fields = field_extra_fields_get_display('botquote', 'botquote', $view_mode);
    foreach ($extra_fields as $name => $settings) {
      if (isset($content[$name])) {
        $content[$name]['#weight'] = $settings['weight'];
        // Visibility: make sure we do not accidentally show a hidden element.
        $content[$name]['#access'] = isset($content[$name]['#access']) ? ($content[$name]['#access'] && $settings['visible']) : $settings['visible'];
      }
    }

    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }
}
