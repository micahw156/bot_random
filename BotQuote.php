<?php

/**
 * Defines BotQuote entity class.
 */
class BotQuote extends Entity {

  /**
   * Defines the entity label if the 'entity_class_label' callback is used.
   */
  protected function defaultLabel() {
    return t('Random quote #@id', ['@id' => $this->identifier()]);
  }

  /**
   * Implements a custom default URI when 'entity_class_uri' is specified as 'uri callback' hook_entity_info().
   */
  protected function defaultUri() {
    return array('path' => 'bot/random/' . $this->identifier());
  }

  /**
   * Returns the raw message text.
   */
  public function getMessage() {
    return isset($this->message) ? $this->message : NULL;
  }
}
