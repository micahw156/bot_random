Bot Random 1.x for Drupal 7.x
=============================

This module is based on the old aidle.tcl script for Eggdrop (circa 1999) by slennox.
The point back then was to keep your bot talking at random intervals so that it wouldn't
be kicked for inactivity. I modified that old script so it would only send out random
quotes on specific weekdays and times to correspond with regularly scheduled family chat
sessions.
