<?php

/**
 * @file
 * Page callbacks for bot_random module.
 *
 * @see bot_random.module
 */

/**
 * Browse all random quotes the bot knows about.
 */
function bot_random_overview() {

  $result = db_query('SELECT * FROM {bot_random_quotes} ORDER BY quote_id DESC')->fetchAll();
  $rows = [];
  foreach ($result as $item) {
    $path = 'bot/random/' . $item->quote_id;
    $actions = [l(t('view'), $path)];
    if (user_access('administer bot') || user_access('author bot quotes')) {
      $actions[] = l(t('edit'), $path . '/edit');
    }
    $row = [
      ['data' => check_plain($item->message), 'class' => ['field-message']],
      ['data' => format_date($item->created, 'custom', 'n/j/Y'), 'class' => ['field-created']],
      ['data' => !empty($item->status) ? t('Y') : t('N'), 'class' => ['field-status']],
      ['data' => implode(' ', $actions), 'class' => ['field-actions']],
    ];
    $rows[] = $row;
  }

  $limit = 100;
  $page = pager_default_initialize(count($rows), $limit, 0);
  $offset = $limit * $page;

  $output = [
    [
      '#theme' => 'table',
      '#header' => [t('Quote'), t('Date'), t('Published'), t('Actions')],
      '#rows' => array_slice($rows, $offset, $limit),
      '#attributes' => ['class' => ['bot-random-overview']],
      '#empty' => t('No results found.'),
    ],
    [
      '#theme' => 'pager',
    ],
  ];
  return $output;
}

/**
 * Page callback to view botquote entity.
 */
function bot_random_view($entity) {
  drupal_set_breadcrumb([l(t('Home'), '<front>'), l(t('Bot'), 'bot'), l(t('Quotes'), 'bot/random')]);
  return entity_view('botquote', [$entity]);
}

/**
 * Create a new schedule item.
 */
function bot_random_add() {
  $entity = entity_create('botquote', []);
  return drupal_get_form('bot_random_form', $entity);
}

/**
 * Entity edit form.
 */
function bot_random_form($form, &$form_state, $entity) {

  if ($entity->is_new) {
    drupal_set_title(t('<em>Create random bot quote</em>'), PASS_THROUGH);
  }
  else {
    drupal_set_title(t('<em>Editing</em> @label', ['@label' => $entity->label()]), PASS_THROUGH);
  }

  $form['entity'] = [
    '#type' => 'value',
    '#value' => $entity,
  ];

  $form['message'] = [
    '#type' => 'textfield',
    '#title' => t('Quote'),
    '#size' => 128,
    '#maxlength' => 512,
    '#default_value' => !empty($entity->message) ? $entity->message : '',
    '#required' => TRUE,
  ];

  $form['status'] = [
    '#type' => 'checkbox',
    '#title' => t('Published'),
    '#default_value' => !empty($entity->status) ? $entity->status : '',
  ];

  $form['author'] = [
    '#type' => 'textfield',
    '#title' => t('Authored by'),
    '#maxlength' => 60,
    '#autocomplete_path' => 'user/autocomplete',
    '#default_value' => !empty($entity->author) ? $entity->author : '',
    '#description' => t('Leave blank for %anonymous.', array('%anonymous' => variable_get('anonymous', t('Anonymous')))),
  ];

  $datevalue = !empty($entity->created) ? $entity->created : REQUEST_TIME;
  $form['created'] = [
    '#type' => 'textfield',
    '#title' => t('Authored on'),
    '#default_value' => $datevalue,
    '#description' => t('This needs work! %date', ['%date' => format_date($datevalue, 'short')]),
    '#element_validate' => array('element_validate_integer_positive'),
  ];

  // Use this to set the weights of the form fields above.
  // @see media_archive_field_extra_fields()
  $extra_fields = field_info_extra_fields('botquote', 'botquote', 'form');
  foreach ($extra_fields as $name => $settings) {
    if (isset($form[$name])) {
      $form[$name]['#weight'] = $settings['weight'];
    }
  }

  field_attach_form('botquote', $entity, $form, $form_state);

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  ];
  return $form;
}

/**
 * Validation handler for form.
 */
function bot_random_form_validate($form, &$form_state) {
  $values = (object) $form_state['values'];
  field_attach_form_validate('botquote', $values, $form, $form_state);
}

/**
 * Submit handler for form.
 */
function bot_random_form_submit($form, &$form_state) {

  $entity = $form_state['values']['entity'];

  $entity->message = $form_state['values']['message'];
  $entity->status = $form_state['values']['status'];
  $entity->created = $form_state['values']['created'];
  $entity->author = $form_state['values']['author'];

  if (isset($entity->author)) {
    if ($account = user_load_by_name($entity->author)) {
      $entity->uid = $account->uid;
    }
    else {
      $entity->uid = 0;
    }
  }

  field_attach_submit('botquote', $entity, $form, $form_state);
  entity_save('botquote', $entity);
  $form_state['redirect'] = $entity->uri();

}

/**
 * Confirmation form to delete a quote.
 */
function bot_random_delete_confirm($form, &$form_state, $entity) {

  $form['entity'] = [
    '#type' => 'value',
    '#value' => $entity,
  ];

  // Always provide entity id in the same form key as in the entity edit form.
  $form['quote_id'] = [
    '#type' => 'value',
    '#value' => $entity->quote_id,
  ];

  $form['preview'] = [
    '#prefix' => '<p>',
    '#markup' => t('Preparing to delete quote #@id: %m', ['@id' => $entity->identifier(), '%m' => $entity->getMessage()]),
    '#suffix' => '</p>',
  ];

  return confirm_form($form,
    t('Are you sure you want to <em>delete</em> this quote?'),
    $entity->uri(), t('This action cannot be undone.'), t('Delete'), t('Cancel')
  );
}

/**
 * Submit handler for delete confirmation.
 */
function bot_random_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm'] == 1 && !empty($form_state['values']['quote_id'])) {
    $entity = $form_state['values']['entity'];
    $num_deleted = db_delete('bot_random_quotes')
      ->condition('quote_id', $form_state['values']['quote_id'])
      ->execute();
    $values = array('%quote_id' => $form_state['values']['quote_id']);
    if ($num_deleted == 1) {
      drupal_set_message(t('Item %quote_id deleted.', $values));
    }
    else {
      watchdog('bot_random', 'Something bad happened deleting item %quote_id', $values, WATCHDOG_ERROR);
    }
  }
  drupal_goto('bot/random');
}
